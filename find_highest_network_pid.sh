#!/bin/bash

# Function to get the PID with the highest network usage
function get_highest_network_pid {
    local network_type=$1
    local command=""
    
    # Check the network type (upload or download) and set the appropriate netstat command
    if [ "$network_type" = "upload" ]; then
        command="netstat -tpn | grep ESTABLISHED | awk '{split(\$5, a, \":\"); print a[1], \$7}' | sort | uniq -c | sort -nr | head -n 1"
    elif [ "$network_type" = "download" ]; then
        command="netstat -tpn | grep ESTABLISHED | awk '{split(\$5, a, \":\"); print a[1], \$7}' | sort | uniq -c | sort -nk1 | tail -n 1"
    else
        echo "Invalid network type. Use 'upload' or 'download'."
        exit 1
    fi
    
    # Execute the netstat command and get the PID with highest network usage
    local result=$(eval "$command")
    local pid=$(echo "$result" | awk '{print $3}')
    local usage=$(echo "$result" | awk '{print $1}')
    
    # Get network statistics for the process with the highest usage
    local network_stats=$(netstat -tnap | grep -E "ESTABLISHED.*$pid/")
    local bytes_transmitted=$(echo "$network_stats" | awk '{sum+=$2} END {print sum}')
    local bytes_received=$(echo "$network_stats" | awk '{sum+=$3} END {print sum}')
    
    # Display the result
    echo "Process with highest network $network_type usage:"
    echo "PID: $pid"
    echo "Network $network_type usage: $usage connections"
    echo "Bytes transmitted: $bytes_transmitted bytes"
    echo "Bytes received: $bytes_received bytes"
}

# Usage example
get_highest_network_pid "upload"
get_highest_network_pid "download"
